Installation
============

Step 1: Edit composer.json
---------------------------

Add to composer.json in your project
 
```json
{
    "require": {
        "tomas-jankus/currency-exchange-bundle": "dev-master"
    }
}
```
... and ...
```json
{
    "repositories": [
        {
            "type": "git",
            "url":  "git@bitbucket.org:TomasDevelop/currency-exchange.git"
        }
    ]
}
```
Then run
```bash
$ composer update
```

Step 2: Enable the Bundle
-------------------------

Then, enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...

            new TomasJankus\CurrencyExchangeBundle\TomasJankusCurrencyExchangeBundle(),
        );

        // ...
    }

    // ...
}
```