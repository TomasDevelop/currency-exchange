<?php

namespace TomasJankus\CurrencyExchangeBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TomasJankus\ExchangerBundle\Service\Exchanger;

class CurrencyRatesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('currency:rates')
            ->setDescription('...')
            ->addArgument('base', InputArgument::REQUIRED, 'Base currency')
            ->addArgument('target', InputArgument::REQUIRED, 'Target currency')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $base = $input->getArgument('base');
        $target = $input->getArgument('target');

        $exchanger = $this->getContainer()->get('tomas_jankus.currency_exchange');

        $rate = $exchanger->getRates($base, $target);

        $output->writeln($rate);
    }
}
