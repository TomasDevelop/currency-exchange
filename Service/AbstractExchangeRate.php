<?php

namespace TomasJankus\CurrencyExchangeBundle\Service;

use Doctrine\Common\Cache\CacheProvider;

abstract class AbstractExchangeRate implements ExchangeRateInterface
{
    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var string
     */
    protected $content;

    /**
     * @var CacheProvider
     */
    private $cache;

    /**
     * @param CacheProvider $cache
     */
    public function setCache(CacheProvider $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @return CacheProvider
     * @throws \Exception
     */
    public function getCache()
    {
        if (!$this->cache instanceof CacheProvider) {
            throw new \Exception("Must set cache provider");
        }
        return $this->cache;
    }

    /**
     * @param $endpoint
     * @return $this
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
        $this->content = null;

        return $this;
    }

    /**
     * @return string
     */
    public function fetch()
    {
        if (!empty($this->content)) {
            return $this->content;
        }

        $cacheKey = md5($this->endpoint);

        $this->content = $this->getCache()->fetch($cacheKey);
        if (!empty($this->content)) {
            return $this->content;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $this->content = curl_exec($ch);

        $this->getCache()->save($cacheKey, $this->content, 3600 * 3);

        return $this->content;
    }

    /**
     * @return mixed
     */
    public function fetchFromJSON()
    {
        $content = $this->fetch();

        return json_decode($content);
    }
}