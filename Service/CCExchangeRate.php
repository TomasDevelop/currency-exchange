<?php

namespace TomasJankus\CurrencyExchangeBundle\Service;

class CCExchangeRate extends AbstractExchangeRate
{
    public function getRate($base, $target)
    {
        $base = strtoupper($base);
        $target = strtoupper($target);

        $query = $base . '_' . $target;

        $endpoint = 'http://free.currencyconverterapi.com/api/v3/convert' .
            '?q=' . $query;

        $content = $this->setEndpoint($endpoint)->fetchFromJSON();

        return $content->results->$query->val;
    }
}
