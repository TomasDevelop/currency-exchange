<?php

namespace TomasJankus\CurrencyExchangeBundle\Service;

class ERLExchangeRate extends AbstractExchangeRate
{
    public function getRate($base, $target)
    {
        $base = strtoupper($base);
        $target = strtoupper($target);

        $endpoint = 'http://api.exchangeratelab.com/api/current' .
            '/' . $base .
            '?apikey=E6827B1786BF9833CCF6F42F5DBB648F';

        $content = $this->setEndpoint($endpoint)->fetchFromJSON();

        foreach ($content->rates as $item) {

            if ($item->to == $target) {
                return $item->rate;
            }
        }

        return 0;
    }
}
