<?php

namespace TomasJankus\CurrencyExchangeBundle\Service;

use Doctrine\Common\Cache\CacheProvider;

interface ExchangeRateInterface
{
    /**
     * @param $base
     * @param $target
     * @return float
     */
    public function getRate($base, $target);

    /**
     * @param CacheProvider $cache
     */
    public function setCache(CacheProvider $cache);

    /**
     * @return CacheProvider
     */
    public function getCache();
}
