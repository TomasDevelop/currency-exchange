<?php

namespace TomasJankus\CurrencyExchangeBundle\Service;

class FixerExchangeRate extends AbstractExchangeRate
{
    public function getRate($base, $target)
    {
        $base = strtoupper($base);
        $target = strtoupper($target);

        $endpoint = 'http://api.fixer.io/latest' .
            '?base=' . $base .
            '&symbols=' . $target;

        $content = $this->setEndpoint($endpoint)->fetchFromJSON();

        return $content->rates->$target;
    }
}
