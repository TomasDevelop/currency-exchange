<?php

namespace TomasJankus\CurrencyExchangeBundle\Service;

use Doctrine\Common\Cache\CacheProvider;
use Monolog\Logger;

class CurrencyExchange
{
    private $rateProviders;

    /**
     * @var CacheProvider
     */
    private $cache;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @param CacheProvider $cache
     */
    public function setCache(CacheProvider $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @return CacheProvider
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    public function __construct()
    {
        $this->rateProviders = array();
    }

    /**
     * @param ExchangeRateInterface $rateProvider
     */
    public function addRateProvider(ExchangeRateInterface $rateProvider)
    {
        $rateProvider->setCache($this->getCache());
        $this->rateProviders[] = $rateProvider;
    }

    /**
     * @return array
     */
    public function getRateProviders()
    {
        return $this->rateProviders;
    }

    /**
     * @param $base
     * @param $target
     * @return array
     * @throws \Exception
     */
    public function getRates($base, $target)
    {
        $rates = [];

        foreach ($this->rateProviders as $rateProvider) {
            try {
                $rate = $rateProvider->getRate($base, $target);
                if (!empty($rate)) {
                    $rates[] = $rate;
                }
            } catch (\Exception $e) {
                $this->logger->addNotice($e->getMessage());
            }
        }

        if (empty($rates)) {
            throw new \Exception("No rates found for a given currency pair");
        }

        return $rates;
    }

    /**
     * @param $base
     * @param $target
     * @return float
     */
    public function getBestRate($base, $target)
    {
        $rates = $this->getRates($base, $target);
        return max($rates);
    }
}