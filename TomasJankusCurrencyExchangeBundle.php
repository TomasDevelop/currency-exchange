<?php

namespace TomasJankus\CurrencyExchangeBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use TomasJankus\CurrencyExchangeBundle\DependencyInjection\Compiler\CurrencyExchangeCompilerPass;

class TomasJankusCurrencyExchangeBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new CurrencyExchangeCompilerPass());
    }
}
