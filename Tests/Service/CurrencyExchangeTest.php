<?php

namespace TomasJankus\CurrencyExchangeBundle\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CurrencyExchangeTest extends WebTestCase
{
    private $currencyExchangeService;

    public function setUp()
    {
        parent::setUp();

        static::bootKernel();

        $container = static::$kernel->getContainer();
        $this->currencyExchangeService = $container->get('tomas_jankus.currency_exchange');
    }

    public function testExchangeRateProviders()
    {
        $providers = $this->currencyExchangeService->getRateProviders();

        $this->assertContainsOnlyInstancesOf(
            '\TomasJankus\CurrencyExchangeBundle\Service\ExchangeRateInterface',
            $providers
        );
    }

    public function testGetRates()
    {
        $rates = $this->currencyExchangeService->getRates('usd', 'eur');

        $this->assertNotEmpty($rates);
    }

    public function testGetBestRate()
    {
        $rate = $this->currencyExchangeService->getBestRate('usd', 'eur');

        $this->assertGreaterThan(0, $rate);
    }
}
