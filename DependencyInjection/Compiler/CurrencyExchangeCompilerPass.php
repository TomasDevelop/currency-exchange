<?php

namespace TomasJankus\CurrencyExchangeBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class CurrencyExchangeCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (false === $container->hasDefinition('tomas_jankus.currency_exchange')) {
            return;
            /*
             * TODO: throw exception
             */
        }

        $definition = $container->getDefinition('tomas_jankus.currency_exchange');

        $providers = $container->findTaggedServiceIds('exchange.rate.provider');

        foreach ($providers as $id => $attributes) {
            $definition->addMethodCall('addRateProvider', array(new Reference($id)));
        }
    }
}
